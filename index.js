const io = require('socket.io')({
    cors: {
        "origin": "*",
    }
});

// function greeting() {
//     return {
//         channel: "Chatroom",
//         content: "Hello",
//         time: new Date(),
//         init: true
//     }
// }

io.on("connection", client => {
    // client.emit("message", greeting());
    client.on("message", data => {
        console.log(data);
        client.broadcast.emit("message", data);
    });
})

io.listen(process.env.PORT || 8000);
