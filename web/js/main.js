let socket = null;


function convertTime(dateISO) {
  return dateISO.substring(11, 16)
}

function connectToServer() {
  socket = io("ws://localhost:8000");
  socket.on("connect", () => { });
  socket.on("message", (data) => {
    receive(data);
  });
}

function send(msg, channel) {
  let message = {
    msg: msg,
    channel: channel,
    time: (new Date()).toISOString()
  };
  socket.emit("message", message);
  message.direction = "send"
  if (!conversation[channel]) {
    conversation[channel] = [];
  }
  conversation[channel].push(message);

}

function receive(msg) {
  if (!msg.init) {
    if (!conversation[msg.channel]) {
      console.log("create new channel in conversation", msg.channel);
      conversation[msg.channel] = [];
    }
    msg.direction = "receive"
    conversation[msg.channel].push(msg);
    renderChannelList();

    if (msg.channel === currentChannel) {
      displayMessage(currentChannel)
    }
  }

}

// must run first to connect the server
connectToServer();
let currentChannel = "";
let conversation = {};


const createChannel = document.getElementById("createChannel");

createChannel.addEventListener("keydown", function (event) {
  if (event.key === "Enter") {
    currentChannel = event.target.value;
    console.log("current channel", currentChannel);
    conversation[currentChannel] = [];
    createChannelDiv(currentChannel, (new Date()).toISOString());
  }
})

function chatScrollToBottom() {
  let chatEl = document.querySelector(".chat-display");
  console.log(chatEl);
  chatEl.scrollTop = chatEl.scrollHeight;
}


function createChannelDiv(channelName, dateISO) {
  let date = convertTime(dateISO)
  // let date = dateObj.getHours() + ":" + dateObj.getMinutes();
  let template = `
    <div id=${channelName} class="channel">
      <div class="channel-upper">
        <div class="channel-name"> ${channelName}</div>
        <div class="channel-time">${date}</div>
      </div>
    </div>
  `;

  const parser = new DOMParser();
  const newDom = parser.parseFromString(template, "text/html");
  const newChannel = newDom.body.firstChild;
  console.log(newChannel);
  newChannel.addEventListener("click", function (event) {
    // currentChannel = event.target.id;
    currentChannel = event.currentTarget.id;
    displayMessage(currentChannel);

  })

  const channelList = document.querySelector(".channel-list");
  channelList.insertBefore(newChannel, channelList.firstChild);
}

function displayMessage(channel) {
  console.log("display");
  console.log(channel);
  console.log(conversation);
  const chatDisplay = document.querySelector(".chat-display");
  chatDisplay.innerHTML = "";
  if (conversation[channel].length > 0) {
    for (let msg of conversation[channel]) {
      let time = convertTime(msg.time);
      let modifiedMsg = `
      <div class="message-wrapper">
        <span class="message-content">${msg.msg}</span>
        <span class="message-time">${time}</span>
        </div>
      `
      if (msg.direction === "send") {
        modifiedMsg = `
        <div class="message-wrapper send">
          <span class="message-time ">${time}</span>
          <span class="message-content send">${msg.msg}</span>
        </div>
        `
      }
      console.log(modifiedMsg);
      chatDisplay.innerHTML += modifiedMsg;
    }
    chatScrollToBottom();
  }

}

function renderChannelList() {
  const channelList = document.querySelector(".channel-list");
  channelList.innerHTML = "";

  console.log("conversation", conversation)
  let lastMessages = [];
  //  for (const [key, value] of Object.entries(conversation)) {
  for (const [channel, msgArr] of Object.entries(conversation)) {

    if (msgArr.length > 0) {
      lastMessages.push(msgArr[msgArr.length - 1])
    } else {
      console.log("here")
      lastMessages.push({
        channel,
        time: "1970-01-31T16:00:00.000Z"
      })
    }
  }
  lastMessages.sort(function (a, b) {
    if (a.time > b.time) {
      return 1
    }
    if (a.time < b.time) {
      return -1
    }
  })
  console.log(lastMessages);

  //loop last Message
  for (let msg of lastMessages) {
    console.log(msg)
    createChannelDiv(msg.channel, msg.time);
  }

}

const chatInput = document.getElementById("chatInput");
chatInput.addEventListener("keydown", function (event) {
  if (event.key === "Enter") {
    send(event.target.value, currentChannel);
    renderChannelList();
    displayMessage(currentChannel);
  }
})


